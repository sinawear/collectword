<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: matengfei
 * Date: 16/7/20
 * Time: 下午3:05
 */

class User_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        //设置表名
        //$this->_table = 'account';
        //$this->pk_name = 'account_id';

        $this->load->database();


    }


    //cookie登陆：检测用户是否登陆，如果cookie值失效，则返回false，如果cookie值未失效，则根据cookie中的用户名和密码从数据库中获取用户信息，如果能获取到用户信息，则返回查询到的用户信息，如果没有查询到用户信息，则返回0
    public function is_login(){
        //获取cookie中的值
        if(empty($_COOKIE['username']) || empty($_COOKIE['password'])){
            $user_info = false;
        }else{
            $user_info=$this->check_user_login($_COOKIE['username'],$_COOKIE['password']);
        }
        return $user_info;
    }


    //根据用户名及加密密码从数据库中获取用户信息，如果能获取到，则返回获取到的用户信息，否则返回false，注意：密码为加密密码
    public function check_user_login($username,$password){
        //这里大家要注意：$password为md5加密后的密码
        //$this->db->query("select * from ");
        //快捷查询类的使用：能为我们提供快速获取数据的方法
        //此数组为查询条件
        //注意：关联数组
        $arr=array(
            'username'=>$username,//用户名
            'password'=>$password,//加密密码
            'status'=>1 //账户为开启状态
        );

        //在database.php文件中已经设置了数据表的前缀，所以此时数据表无需带前缀
        $query = $this->db->get_where("t_users",$arr);
        //返回二维数组
        //$data=$query->result_array();
        //返回一维数组
        $user_info=$query->row_array();
        if(!empty($user_info)){
            return $user_info;
        }else{
            return false;
        }
    }


}