<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class layout2
{
    var $obj;
    var $layout;

    function layout2($params = array())
    {
        $this->obj =& get_instance();
        if (count($params) > 0) {
            $this->initialize($params);
        } else {
            $this->layout = 'default/layout_main';
        }
    }

    function initialize($params = array())
    {
        if (count($params) > 0) {
            foreach ($params as $key => $val) {
                $this->$key = $val;
            }
        }
    }

    function view($view, $data = null, $return = false)
    {
        echo $view;
        $data['content_for_layout'] = $this->obj->load->view($view, $data, true);
        var_dump($data);
        //echo $data['content_for_layout'];
        if ($return) {
            $output = $this->obj->load->view($this->layout, $data, true);
            return $output;
        } else {
            $this->obj->load->view($this->layout, $data, false);
        }
    }
}

?>