<?php

//核心模型扩展
class REST_Model extends CI_Model
{
    var $t = '';

    //构造方法
    public function __construct()
    {
        parent::__construct();
    }

    //设置表
    public function setTable($table)
    {
        $this->t = $table;
    }

    public function getTable()
    {
        return $this->t;
    }

    //字段过滤
    public function fieldFilter($table, $data)
    {
        if (isset($data['id'])) unset($data['id']);

        $fields = $this->db->list_fields($table);

        foreach ($data as $key => $value) {
            if (!in_array($key, $fields)) {
                unset($data[$key]);
            }
        }
        return $data;
    }

    /**
     * 插入数据
     * @param  $table
     * @param  $data
     */
    public function insert($table, $data)
    {
        $data = $this->fieldFilter($table, $data);
        $r = $this->db->insert($table, $data);
        if ($r) {
            return $this->db->insert_id();
        } else {
            return $r;
        }
    }

    /**
     *修改表数据
     * @param  $table
     * @param  $data
     * @param  $condition
     */
    public function update($table, $data, $condition)
    {
        $data = $this->fieldFilter($table, $data);
        return $this->db->update($table, $data, $condition);
    }

    /**
     * 删除表数据
     * @param  $table
     * @param  $condition
     */
    public function delete($table, $condition)
    {
        return $this->db->delete($table, $condition);
    }

    /**
     * 根据sql语句直接查询
     * @param  $sql
     */
    public function query($sql)
    {
        $rs = $this->db->query($sql);
        if (gettype($rs) == 'boolean') {
            return $rs;
        } else if (gettype($rs) == 'object') {
            return $rs->result_array();
        }
    }

    /**
     * 根据条件选取表数据.
     * @param  $table 表名
     * @param  $condition where条件
     * @param  $row 选取表字段
     * @param  $limit 记录条数
     * @param  $offset 记录偏移量
     * @param  $order 排序字段
     * @param  $sort 排序方式
     */
    public function selectCon($table, $condition, $row = '', $limit = '', $offset = '', $order = '', $sort = 'desc', $like = '')
    {
        if ($condition) $this->db->where($condition);

        if ($limit) $this->db->limit($limit);
        if ($offset) $this->db->limit($limit, $offset);
        if ($row) $this->db->select($row);
        if (is_array($like)) {
            if ($like['like']) $this->db->like($like['like']);
            if ($like['orlike']) $this->db->or_like($like['orlike']);
        }
        if ($order) $this->db->order_by($order, $sort);
        $rs = $this->db->get($table);
        if (gettype($rs) == 'boolean') {
            return $rs;
        } else if (gettype($rs) == 'object') {
            return $rs->result_array();
        }
    }

    /**
     * 选取整个表数据
     * @param  $table 表名
     * @param  $order 排序字段
     * @param  $sort   排序方式
     * @return  数值
     */
    public function selectAll($table, $order = '', $sort = 'desc')
    {
        if ($order) {
            $this->db->order_by($order, $sort);
        }
        $rs = $this->db->get($table);
        if (gettype($rs) == 'boolean') {
            return $rs;
        } else if (gettype($rs) == 'object') {
            return $rs->result_array();
        }
    }

    //查询指定条件返回行数
    public function getNums($table, $condition = FALSE, $like = FALSE)
    {
        if ($condition) {
            $this->db->where($condition);
        }
        if (is_array($like)) {
            if ($like['like']) $this->db->like($like['like']);
            if ($like['orlike']) $this->db->or_like($like['orlike']);
        }
        $query = $this->db->get($table);
        $nums = $query->num_rows();
        return "$nums";
    }

    //判断数组是否为空
    public function is_empty($arr)
    {
        $arr = array_filter($arr);
        if (empty($arr)) {
            return 0;
        } else {
            return 1;
        }
    }
}
