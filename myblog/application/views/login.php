<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!--
如何防止用户直接访问该页面呢?

-->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>登录</title>

    <style type="text/css">

        ::selection { background-color: #E13300; color: white; }
        ::-moz-selection { background-color: #E13300; color: white; }


        #div_content {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;

            text-align:center;

        }


        #title{
            width: 100%;
            text-align:center;
        }

        #div_content{
            text-align:center;
        }

    </style>

    <script type="text/javascript">


    </script>


</head>
<body>

<div id="container">

    <div id="title">
        <h1>数据管理系统</h1>
    </div>

    <div id="body">

        <div id="div_content">
            <form action="<?php echo site_url('login')?>" method="post">
            账号:<input type="text" name="username" placeholder="账号"/>
            <br />
            密码:<input type="password" name="password" placeholder="密码" />
            <br />
            <input type="submit" id="go" value="登录"/>

            </form>

        </div>



    </div>

</div>

</body>
</html>