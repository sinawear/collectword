<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>我的知识库</title>

    <style type="text/css">

        ::selection { background-color: #E13300; color: white; }
        ::-moz-selection { background-color: #E13300; color: white; }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }



        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }



        #body {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;

            height:1000px;
        }

        #left{
            float:left;

            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            padding: 12px 10px 12px 10px;


        }

        #right{
            float:right;

            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            padding: 12px 10px 12px 10px;

        }

        textarea{
            width:400px;
            height:500px;

        }

        #title{
            width:400px;
        }
        #container {

        }
    </style>

    <script type="text/javascript">

        var line_row ;

        function preview_dir(){
            var obj = document.getElementById("content_dir");
            console.log(obj.value);
            //alert(obj.value);

            var str = obj.value;

            /*
            //var str = obj.value.replace('“',"\"").replace('”',"\""); // 替换掉中文引号
            //解析数据编译成json字符串
            var obj_dir = JSON.parse(str);
            console.log(obj.dir);
            */

            line_row = 0;

            //read_line_data(str);
            var root = new Object();
            rr(root, str);

            var obj_dir = JSON.parse(root);
            console.log(obj_dir);


        }

        function rr(obj,str){

            if(obj == null) return;

            var line_data = read_line_data(str);
            obj.childNum = line_data.child_num;
            obj.name = line_data.name;

            if(obj.childNum > 0){
                var array = new Array();
                for(var i = 0; i<obj.childNum; i++){
                    var child = new Object();
                    rr(child);
                    array[i]=child;
                }
                obj.next = array;
            }
             

        }


        function read_line_data(str){

            var array = my_split(str, '\n'); //str.split("\\n");
            var str_sub =  array[line_row++];

            // 解析单行文件
            var pos_start = str_sub.indexOf("[");
            var pos_end = str_sub.indexOf("]");

            var obj = new Object();
            obj.child_num = str_sub.substring(pos_start+1,pos_end);
            obj.name = str_sub.substring(pos_end+1, str_sub.length);
            obj.next = null;

            return obj;

        }


        function my_split(str, flag){
            var array = new Array();
            var row = 0;
            var pos = 0;
            var last_pos = -1;
            for(var i = 0; i<str.length; i++){
                var c = str.charAt(i);      console.log(c);
                if(c == flag){
                    pos = i;
                    array[row++] = str.substring(last_pos+1, pos).trim();
                    last_pos = pos;
                }
            }

            if(last_pos != -1){  // 当字符串到达最末尾当时候,会跳出导致漏掉最后一组
                array[row] = str.substring(last_pos+1, str.length).trim();
            }

            return array;


        }






    </script>


</head>
<body>

<div id="container">
    <h1>后台</h1>



    <div id="body">

        <div id="left">
            目录:<textarea type="text" id="content_dir" ></textarea>
            <br />
            <input type="button" id="go" value="预览" onclick="preview_dir()" />
        </div>

        <div id="right">
            <form action="">
                标题:<input type="text" id="title" placeholder="标题"/>
                <br />
                内容:<textarea type="text" id="content" ></textarea>
                <br />
                <input type="submit" id="go" value="确认按钮"/>

            </form>

        </div>



    </div>

</div>

</body>
</html>