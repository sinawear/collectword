<?php

/**
 * Created by PhpStorm.
 * User: matengfei
 * Date: 16/1/14
 * Time: 下午12:12
 */
class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model');
    }


    public function index()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        var_dump($username) ;

        //如果用户名和密码为空，则返回登陆页面
        if (empty($username) || empty($password)) {
            $data['verifycode'] = rand(1000, 9999);//生成一个四位数字的验证码
            //将验证码放入session中,注意：参数是数组的格式
            $this->session->set_userdata($data);
            //注意：CI框架默认模板引擎解析的模板文件中变量不需要$符号
            //$this->parser->parse("admin/login",$data);
            //smarty模板变量赋值
            //$this->tp->assign("verifycode", $data['verifycode']);
            //ci框架在模板文件中使用原生态的PHP语法输出数据
            $this->load->view('login',$data);//登陆页面，注意：参数2需要以数组的形式出现
            //显示smarty模板引擎设定的模板文件
            //$this->tp->display("admin/login.php");
        } else {
            $username = isset($username) && !empty($username) ? trim($username) : '';//用户名
            $password = isset($password) && !empty($password) ? trim($password) : '';//密码
            $verifycode = isset($_POST['verifycode']) && !empty($_POST['verifycode']) ? trim($_POST['verifycode']) : '';//验证码


            //做验证码的校验
            //if ($verifycode == $this->session->userdata('verifycode'))
            if(TRUE)
            {
                //根据用户名及密码获取用户信息，注意：参数2是加密的密码
                $user_info = $this->user_model->check_user_login($username, md5($password));
                //var_dump($user_info);
                if ($user_info['user_id'] > 0) {
                    //将用户id、username、password放入cookie中
                    //第一种设置cookie的方式：采用php原生态的方法设置的cookie的值
                    //setcookie("user_id",$user_info['user_id'],86500);
                    //setcookie("username",$user_info['username'],86500);
                    //setcookie("password",$user_info['password'],86500);
                    //echo $_COOKIE['username'];

                    //第二种设置cookie的方式：通过CI框架的input类库
                    $this->input->set_cookie("username", $user_info['username'], 3600);
                    $this->input->set_cookie("password", $user_info['password'], 3600);
                    $this->input->set_cookie("user_id", $user_info['user_id'], 3600);
                    //echo $this->input->cookie("password");//适用于控制器
                    //echo $this->input->cookie("username");//适用于控制器
                    //echo $_COOKIE['username'];//在模型类中可以通过这种方式获取cookie值
                    //echo $_COOKIE['password'];//在模型类中可以通过这种方式获取cookie值

                    //第三种设置cookie的方式：通过CI框架的cookie_helper.php函数库文件
                    //这种方式不是很灵验，建议大家采取第二种方式即可
                    //set_cookie("username",$user_info['username'],3600);
                    //echo get_cookie("username");

                    //session登陆时使用：将用户名和用户id存入session中
                    $data['username']=$user_info['username'];
                    $data['user_id']=$user_info['user_id'];
                    $this->session->set_userdata($data);

                    //跳转到指定页面
                    //注意：site_url()与base_url()的区别，前者带index.php,后者不带index.php
                    header("location:" . site_url("home"));
                }
                else{
                    echo "账号密码错误";
                }
            } else {
                //跳转到登陆页面
                header("location:" . site_url("index"));

            }
        }
    }


}