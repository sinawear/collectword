<?php

class Mlogin extends REST_Model
{
    function __construct()
    {
        parent::__construct();
    }


    public function add($data)
    {
        $log = $this->insert('t_login', $data);
        return $log;
    }


    public function get($id_umeng)
    {

        $condition = array('id_umeng' => $id_umeng);
        $result = $this->selectCon('t_login', $condition);

        return $result;
    }

    public function login($data)
    {

        $condition = array('id_umeng' => $data['id_umeng']);
        $result = $this->selectCon('t_login', $condition);
        if ($result)
            $res = $this->update('t_login', $data, $condition);
        else
            $res = $this->insert('t_login', $data);
        return $res;

    }


}

?>