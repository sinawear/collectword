<?php

class Pages
{

    private $each_disNums;//每页显示的条目数

    private $nums;//总条目数

    private $current_page;//当前被选中的页

    private $sub_pages;//每次显示的页数

    private $pageNums;//总页数

    private $page_array = array();//用来构造分页的数组

    private $subPage_link;//每个分页的链接

    private $start;//起始页

    function Page($each_disNums, $nums)
    {


        if (!$each_disNums) {
            $this->each_disNums = 1;
        } else {
            $this->each_disNums = intval($each_disNums);
        }
        if (!$nums) {
            $this->nums = 0;
            $this->pageNums = 0;
        } else {
            $this->nums = intval($nums);
            $this->pageNums = ceil($nums / $each_disNums);
        }

        $this->current_page = @$_GET["p"] ? @$_GET["p"] : 1;

        $this->sub_pages = 5;


        $this->subPage_link = "?p=";

        $page_view = $this->PageCss();
        $this->start = ($this->current_page - 1) * $each_disNums;

        $data = array('start' => $this->start, 'page' => $page_view);

        return $data;
        //print_r($data);
    }


    /*

      __destruct析构函数，当类不在使用的时候调用，该函数用来释放资源。

     */

    function __destruct()
    {

        unset($each_disNums);

        unset($nums);

        unset($current_page);

        unset($sub_pages);

        unset($pageNums);

        unset($page_array);

    }

    /*

      用来给建立分页的数组初始化的函数。

     */

    function initArray()
    {

        for ($i = 0; $i < $this->sub_pages; $i++) {

            $this->page_array[$i] = $i;

        }

        return $this->page_array;

    }


    /*

      construct_num_Page该函数使用来构造显示的条目

      即使：[1][2][3][4][5][6][7][8][9][10]

     */

    function construct_num_Page()
    {

        if ($this->pageNums < $this->sub_pages) {

            $current_array = array();

            for ($i = 0; $i < $this->pageNums; $i++) {

                $current_array[$i] = $i + 1;

            }

        } else {

            $current_array = $this->initArray();

            if ($this->current_page <= 3) {

                for ($i = 0; $i < count($current_array); $i++) {

                    $current_array[$i] = $i + 1;

                }

            } elseif ($this->current_page <= $this->pageNums && $this->current_page > $this->pageNums - $this->sub_pages + 1) {

                for ($i = 0; $i < count($current_array); $i++) {

                    $current_array[$i] = ($this->pageNums) - ($this->sub_pages) + 1 + $i;

                }

            } else {

                for ($i = 0; $i < count($current_array); $i++) {

                    $current_array[$i] = $this->current_page - 2 + $i;

                }

            }

        }
        return $current_array;

    }


    /*

     当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页]

     */

    function PageCss()
    {

        $subPageCss2Str = "";

        $subPageCss2Str .= "<div style='height:27px;float:right;margin-top:10px;font-size:14px;align:right;'><span  style='width:200px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:20px;'>当前第" . $this->current_page . "/" . $this->pageNums . "页</span>  ";


        if ($this->current_page > 1) {

            $firstPageUrl = $this->subPage_link . "1";

            $prewPageUrl = $this->subPage_link . ($this->current_page - 1);

            $subPageCss2Str .= "<a href='$firstPageUrl' style='text-decoration: none;'><span style='border:1px solid #D0D0D2;width:40px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:5px;cursor:pointer;'>首页</span></a> ";

            $subPageCss2Str .= "<a href='$prewPageUrl' style='text-decoration: none;'><span style='border:1px solid #D0D0D2;width:50px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:5px;cursor:pointer;'>上一页</span></a> ";

        } else {

            $subPageCss2Str .= "<span style='border:1px solid #D0D0D2;width:40px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:5px;'>首页</span> ";

            $subPageCss2Str .= "<span style='border:1px solid #D0D0D2;width:50px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:5px;'>上一页</span> ";

        }


        $a = $this->construct_num_Page();

        for ($i = 0; $i < count($a); $i++) {

            $s = $a[$i];

            if ($s == $this->current_page) {

                $subPageCss2Str .= "<span style='border:1px solid #D0D0D2;width:20px;height:20px;text-align:center; line-height:20px; color:#E0DEDE;display:block;background:#7E3131;float:left;margin-right:5px;'>" . $s . "</span>  ";

            } else {

                $url = $this->subPage_link . $s;

                $subPageCss2Str .= "<a href='$url' style='text-decoration: none;'><span style='border:1px solid #D0D0D2;width:20px;height:20px;text-align:center; line-height:20px; color:#626262;display:block;float:left;margin-right:5px;cursor:pointer;'>" . $s . "</span></a>  ";
            }

        }


        if ($this->current_page < $this->pageNums) {

            $lastPageUrl = $this->subPage_link . $this->pageNums;

            $nextPageUrl = $this->subPage_link . ($this->current_page + 1);

            $subPageCss2Str .= "<a href='$nextPageUrl' style='text-decoration: none;'><span style='border:1px solid #D0D0D2;width:50px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:5px;cursor:pointer;'>下一页</span></a> ";

            $subPageCss2Str .= "<a href='$lastPageUrl' style='text-decoration: none;'><span style='border:1px solid #D0D0D2;width:40px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:50px;cursor:pointer;'>尾页</span></a></div>";

        } else {

            $subPageCss2Str .= "<span style='border:1px solid #D0D0D2;width:50px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:5px;'>下一页</span> ";

            $subPageCss2Str .= "<span style='border:1px solid #D0D0D2;width:40px;height:20px;text-align:center; line-height:20px;color:#626262;display:block;float:left;margin-right:50px;'>尾页</span></div>";

        }

        return $subPageCss2Str;

    }

}

?>