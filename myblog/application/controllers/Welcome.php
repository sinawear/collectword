<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->output->enable_profiler(TRUE);

		$this->load->helper('form');
		$this->load->helper('url');

		//
		$action_bar = array("html", "android");


		$r1_0 = array('JSON相关类介绍');
		$r1_1 = array('JSON Object对象', 'JSON Object对象的创建','JSON object的主要方法');
		$r1_2 = array('JSON ARRAY对象', 'JSON Array对象的创建','JSON Array的主要方法');
		$r1_3 = array("JSON工具库的使用",'将JSON转换成字符串');
		$data['list_container_left'] = array($r1_0, $r1_1, $r1_2, $r1_3);
		$data['title'] = "标题";
		$data['content'] = array(array('1', '2'));
		$data['base_url'] = "";
		$data['action_bar'] = $action_bar;

		// 读取文章
		$data['pagesize'] = 10;

		//$data['list_container_left'] = array('JSON相关类介绍',"JSON Object对象", "JSON ARRAY对象", "JSON工具库的使用");




		$this->load->view('welcome_message', $data);
	}

	public function list_detail(){

		$r1_0 = array('JSON相关类介绍');
		$r1_1 = array('JSON Object对象', 'JSON Object对象的创建','JSON object的主要方法');
		$r1_2 = array('JSON ARRAY对象', 'JSON Array对象的创建','JSON Array的主要方法');
		$r1_3 = array("JSON工具库的使用",'将JSON转换成字符串');
		$data['list_container_left'] = array($r1_0, $r1_1, $r1_2, $r1_3);
		$data['title'] = "标题";
		$data['content'] = array(array('1', '2'));

		$this->load->view("list_detail", $data);
	}

}
