<?php

//无需验证控制器
class Tjc_Controller extends CI_Controller
{

    var $nav, $footer, $user, $res;

    //构造方法
    public function __construct()
    {
        parent::__construct();
        header('Content-type:text/html;charset=UTF-8');
        //加载项目函数库
        $this->load->helper(array('common', 'url', 'cookie'));
        $this->load->library('session1');

        //加载cache适配器
        $this->load->driver('cache', array('adapter' => 'file'));

        //调试 优化
        if (ENVIRONMENT == 'development' && isset($_GET['tapi'])) {
            $this->output->enable_profiler(TRUE);
        }
        //获取缓存文件   liub 2013-5-17
        $this->getCommCache();
        $this->user = $this->getSession('user');
        if (empty($this->user)) {
            $this->get_user();
        }

    }

    //验证登录用户合法信息
    public function checkUserFromLogin()
    {
        $user = $this->getSession('user');
        if ($user['info']['id'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    //获取用户car商品数量
    public function getCarNums()
    {
        $this->load->model('Mcar');
        $mark = $this->getSession('car');
        $user_id = $this->getSession('user');
        @$uid = $user_id['uid'];
        if ($user_id) {
            $result = @$this->Mcar->get_car_by_uid(@$user_id['uid']);
            $car = array('list' => $result);
            $res = count($result);
        } else {
            $result = $mark;
            $car = array('list' => $result);
            if (!$result) {
                $res = 0;
            } else {
                $res = count($result);
            }
        }
        return $res;
    }

    //获取访问用户端信息
    public function getClientUserInfo()
    {
        $this->load->library('mobile');
        $layout = ($this->mobile->isMobile() ? ($this->mobile->isTablet() ? 'tablet' : 'mobile') : 'desktop');//判断终端类型
        //设备类型
        return $layout;
    }

    //验证Api用户合法信息
    public function checkUserFromApi()
    {

    }

    /**
     * 获取公共缓存nav help
     *
     * @copyright  2011-2013  Digirun.cn
     * @since      File available since Release 1.0 -- 2013-5-17 下午04:03:01
     * @author liub
     *
     */
    public function getCommCache()
    {
        //获取nav
        if ($this->getCache('nav')) {
            $this->nav = $this->getCache('nav');
        } else {
            $this->load->model('Mhead');
            $this->nav = $this->Mhead->get_head();
            $this->setCache('nav', $this->nav, 10);//三个小时
        }
        //获取分类category
        if ($this->getCache('category')) {
            $this->category = $this->getCache('category');
        } else {
            $this->load->model('Mhead');
            $category = $this->Mhead->get_category(0, 5);//获取五条分类信息
            foreach ($category as $k => $val) {
                $cat = $this->Mhead->get_category($val['cat_id'], 8);//获取八条二级分类
                $category[$k]['cate'] = $cat;
            }
            $this->category = $category;
            $this->setCache('category', $this->category, 10);//三个小时
        }
        //获取help
        if ($this->getCache('footer')) {
            $this->footer = $this->getCache('footer');
        } else {
            $this->load->model('Marticle');
            $foots = array();
            $foot = $this->Marticle->get_article_cat();
            $id = array('n_one', 'n_two', 'n_three', 'n_four', 'n_five', 'n_six', 'n_seven', 'n_eight');
            for ($i = 0; $i < count($foot); $i++) {
                $foot1 = $this->Marticle->get_article_by_id(@$foot[$i]['cat_id']);
                array_push($foots, array(
                    'cat_name' => @$foot[$i]['cat_name'],
                    'title' => $foot1,
                    'note' => @$id[$i],
                ));
            }
            $this->footer = $foots;
            $this->setCache('footer', $this->footer, 10);//三个小时
        }
    }

    //文件缓存
    public function setCache($id, $data, $time = null)
    {
        $this->cache->save($id, $data, $time);
    }

    //文件缓存获取
    public function getCache($id)
    {
        return $this->cache->get($id);
    }

    //文件缓存删除
    public function delCache($id)
    {
        $this->cache->delete($id);
    }

    //session
    public function setSession($key, $session)
    {
        $this->session1->set($key, $session);
    }

    //获取session
    public function getSession($key)
    {
        return $this->session1->get($key);
    }

    //删除session
    public function delSession($key)
    {
        return $this->session1->delete($key);
    }

    //日志
    public function setUserLog($data)
    {
        $this->load->model('Worker_log');
        return $this->Worker_log->addWorkerLog($data);
    }

    //mark
    public function getMark()
    {
        $mark = $this->getSession('mark');
        if ($mark) {
            return $mark;
        } else {
            $mark = time();
            $this->setSession('mark', $mark);
        }
    }

    //cookie用来保存用户名/uname
    public function getUserName()
    {
        return $_COOKIE['uname'];
    }

    //设置用户名，不一样删除
    public function setUserName($uname)
    {
        @$cname = $_COOKIE['uname'];
        if ($cname) {
            setcookie('uname', $uname, 0);
        }
        if ($cname != $uname) {
            setcookie('uname', $uname);
        }
    }

    public function get_user()
    {
        $acl = $this->uri->segment(1);
        $mol = $this->uri->segment(2);
        if ($acl == 'user' && $mol != 'login_in' && $mol != 'register') {
            echo "<script>location.href='" . site_url('user/login_in') . "'</script>";
        } elseif ($acl == 'pad_address' || $acl == 'pad_comment' || $acl == 'pad_favorite') {
            echo "<script>location.href='" . site_url('pad_home') . "'</script>";
        }
    }
}

//检测登录设备
class Tjc_ClientConetoller extends Tjc_Controller
{
    public function __construct()
    {
        parent::__construct();
        //获取设备信息
        $layout = $this->getClientUserInfo();
        if ($layout == 'desktop') {
            redirect('home');
        } elseif ($layout == 'tablet') {
            redirect('pad_home');
        } else {
            redirect('mob_home');
        }
    }


}

/**
 * web加载页头页脚
 *
 * @copyright  2011-2013  Digirun.cn
 * @since      File available since Release 1.0 -- 2013-5-20 下午05:46:40
 * @author liub
 */
class Tjc_WebController extends Tjc_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('layout');
        $this->res = $this->getCarNums();
        $this->layout->setSlot('common/head', array('nav' => $this->nav, 'user' => $this->user, 'res' => $this->res, 'category' => $this->category));
        $this->layout->setSlot('common/footer', array('footer' => $this->footer));
    }
}

//需验证mark ,登录
class Tjc_NotifyController extends Tjc_Controller
{
    var $user;
    var $mark;

    //构造方法
    public function __construct()
    {
        parent::__construct();
        //获取用户信息
        $this->mark = $this->getMark();
    }

    //获取用户访问方式
    public function getUserFrom()
    {
        if (!empty($this->user['info']['id'])) {
            return 1;
        } else if ($this->input->get('t') !== false) {
            return 2;
        } else {
            return false;
        }
    }


    //日志
    public function setLog($action, $v)
    {
        $this->load->model('Worker_log');
        $data['ip'] = $this->user['loginInfo']['ip'];
        $data['time'] = mktime();
        $data['action'] = $action;
        $data['value'] = $v;
        $data['worker'] = $this->user['info']['id'];
        $data['from'] = $this->user['loginInfo']['from'];

        return $this->Worker_log->addWorkerLog($data);
    }

}

