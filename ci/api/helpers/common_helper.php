<?php

/**
 * delete element from an array by keys
 * 从键值对的数组中删除一些键值
 * @param array $_arr
 * @param array $_del elements to be deleted
 * @return array
 */
function array_delete($_arr, $_keys)
{
    $arr = array();
    foreach ($_arr as $key => $value) {
        if (!in_array($key, $_keys)) {
            $arr[$key] = $value;
        }
    }
    return $arr;
}

/**
 * delete_array
 * 从非键值对的数组中删除几个值
 * @param mixed $_arr
 * @param mixed $_sub
 * @access public
 * @return void
 */
function delete_array($_arr, $_sub)
{
    $arr = array();
    foreach ($_arr as $val) {
        if (!in_array($val, $_sub)) {
            $arr [] = $val;
        }
    }
    return $arr;
}


/**
 * dayw
 * 获取中文日期 带星期
 * @param int $_time 时间戳
 * @access public
 * @return void
 */
function dayw($_time, $_format = 'Y年m月d日')
{
    $arr = a('日', '一', '二', '三', '四', '五', '六');
    $w = date('w', $_time);
    return date($_format, $_time) . ' 星期' . $arr[$w];
}

/**
 *
 * 返回非键值队表现形式的数组
 * @access public
 * @return void
 */
function a()
{
    $args = func_get_args();
    return $args;
}


/**
 * gbk_utf8
 * gbk编码转为utf8
 * @param mixed $_str
 * @access public
 * @return void
 */
function gbk_utf8($_str)
{
    $cur_encoding = mb_detect_encoding($_str);
    if ($cur_encoding == "UTF-8" && mb_check_encoding($_str, "UTF-8"))
        return $_str;
    else return mb_convert_encoding($_str, "UTF-8", "GBK");
    //return mb_convert_encoding($_str, "UTF-8", "GBK");
}

/**
 * csubstr
 * 中文字符
 * @param mixed $str
 * @param mixed $length
 * @param int $start
 * @param string $charset
 * @param mixed $suffix
 * @access public
 * @return void
 */
function csubstr($str, $start = 0, $length, $charset = 'utf-8', $suffix = TRUE)
{
    if (function_exists('mb_substr')) {
        if (mb_strlen($str, $charset) <= $length) return $str;
        $slice = mb_substr($str, $start, $length, $charset);
    } else {
        $re['utf-8'] = '/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/';
        $re['gb2312'] = '/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/';
        $re['gbk'] = '/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/';
        $re['big5'] = '/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/';
        preg_match_all($re[$charset], $str, $match);
        if (count($match[0]) <= $length) return $str;
        $slice = join('', array_slice($match[0], $start, $length));
    }
    if ($suffix) return $slice . '...';
    return $slice;
}

//截取字符串函数
function str_cut($str, $sublen, $etc = '...')
{
    if (strlen($str) <= $sublen) {
        $rStr = $str;
    } else {
        $I = 0;
        while ($I < $sublen) {
            $StringTMP = substr($str, $I, 1);

            if (ord($StringTMP) >= 224) {
                $StringTMP = substr($str, $I, 3);
                $I = $I + 3;
            } elseif (ord($StringTMP) >= 192) {
                $StringTMP = substr($str, $I, 2);
                $I = $I + 2;
            } else {
                $I = $I + 1;
            }
            $StringLast[] = $StringTMP;
        }

        $rStr = implode('', $StringLast) . $etc;
    }

    return $rStr;
}

/**
 * down_load
 * 下载图片
 * @param mixed $_url
 * @param mixed $_path
 * @param mixed $_name
 * @access public
 * @return void
 */
function down_load($_url, $_path, $_name)
{
    $get_file = @file_get_contents($_url);
    $t = explode('.', $_url);
    $ext = array_pop($t);
    $dest = $_path . $_name . '.' . $ext;
    if ($get_file) {
        $fp = @fopen($dest, 'w');
        @fwrite($fp, $get_file);
        @fclose($fp);
    }
}

/*straddslashes
 * 魔术引号关闭中 给数组增加转义
  * @param mixed $dat
 */
function straddslashes($dat)
{
    foreach ($dat as &$v) {
        if (!empty($v))
            $v = addslashes(stripslashes($v));
    }
    return $dat;
}

/*get_client_ip
 * 获取客户端IP地址
 */
function get_client_ip()
{
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
        $ip = getenv("HTTP_CLIENT_IP");
    else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
        $ip = getenv("REMOTE_ADDR");
    else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
        $ip = $_SERVER['REMOTE_ADDR'];
    else
        $ip = "unknown";
    return ($ip);
}

/**
 * 创建文件夹
 * @param $updir 文件目录
 * @param $type 根据日期创建文件夹下目录 1:年/月/日; 2:年/月; 3:年/; 其他不创建
 * @return $upload_path 最终文件目录路径
 */
function createFile($updir, $type)
{
    if ($updir == '') $updir = "./goods_images/";//上传目录
    //设置路径方式
    switch ($type) {
        case '1':
            $m_dir = date('Y') . "/" . date('m') . "/" . date('d') . "/";
            break;
        case '2':
            $m_dir = date('Y') . "/" . date('m') . "/";
            break;
        case '3':
            $m_dir = date('Y') . "/";
        default:
            $m_dir = '';
            break;
    }
    //设置上传的路径
    $upload_path = $updir . $m_dir;
    //建立文件夹
    if (!is_dir($upload_path)) {
        $temp = explode('/', $upload_path);
        $cur_dir = '';
        for ($i = 0; $i < count($temp); $i++) {
            $cur_dir .= $temp[$i] . '/';
            if (!is_dir($cur_dir)) {
                @mkdir($cur_dir, 0777);
                @fopen("$cur_dir/index.htm", "a");
            }
        }
    }
    return $upload_path;
}


/*----------------快捷键操作区--------------------------------------------------------------------------------------------------*/

/*D
 *查看数据值 快捷操作
 * @param mixed $dat
 */
function D($dat)
{
    die(var_dump($dat));
}

/*R
 *返回指定格式数据
 * @param mixed $dat
 */
function R($dat, $fix = 'json')
{
    if ($fix == 'json') {
        return json_encode($dat);
    }
}

