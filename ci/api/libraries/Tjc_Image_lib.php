<?php

/**
 * 图片处理类
 * @author lb
 */
class Tjc_Image_lib extends CI_Image_lib
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * 创建柱状图
     * @param  $allnums
     * @param  $arr
     */
    function createBar($allnums, $arr)
    {
        $w = 1100;
        $h = 750;
        $ratio = $allnums / 600;
        $font = dirname($_SERVER['SCRIPT_FILENAME']) . '/simsun.ttc';//字体路径
        $url = dirname($_SERVER['SCRIPT_FILENAME']) . '/images/tj/ieambar.png';
        $img = imagecreate($w, $h);
        imageColorAllocate($img, 240, 240, 240);//底色
        $col = imageColorAllocate($img, 0, 0, 0);//线条颜色
        imageSetThickness($img, 1);
        imageLine($img, 50, 700, 50, 50, $col);
        imageLine($img, 50, 50, 40, 60, $col);
        imageLine($img, 50, 50, 60, 60, $col);
        imageLine($img, 50, 700, 1050, 700, $col);
        imageLine($img, 1040, 690, 1050, 700, $col);
        imageLine($img, 1040, 710, 1050, 700, $col); //画xy轴
        for ($i = 0; $i <= 600; $i = $i + 100) {
            imageLine($img, 45, (700 - $i), 50, (700 - $i), $col);
            imagettftext($img, 12, 0, 30, (698 - $i), $col, $font, intval($i * $ratio));
            imagettftext($img, 12, 0, 60, 60, $col, $font, "数量");
            imagettftext($img, 12, 0, 1040, 710, $col, $font, "题型");
        }
        imageSetThickness($img, 30);
        $j = 70;
        foreach ($arr as $son) {
            foreach ($son as $key => $nums) {
                $color = imageColorAllocate($img, rand(0, 255), rand(0, 255), rand(0, 255));
                $tratio = $nums / $allnums;
                $th = 600 * $tratio;
                $text = $key . '(' . $nums . ')';
                imageLine($img, $j + 20, 700, $j + 20, 700 - $th, $color);
                imagettftext($img, 10, 0, $j - 10, 690 - $th, $col, $font, $text);
            }
            $j += 70;
        }
        imagepng($img, $url);
        imageDestroy($img);
    }

    /**
     * 创建饼图...
     * @param  $allnums
     * @param  $arr
     */
    function createPie($allnums, $arr)
    {
        $w = 1000;//宽
        $h = 700;//高
        //创建图像
        $img = imagecreate($w, $h);
        imageColorAllocate($img, 240, 240, 240);//底色
        $col = imagecolorallocate($img, 0, 0, 0);
        //imageSetThickness($img,10);
        $font = dirname($_SERVER['SCRIPT_FILENAME']) . '/simsun.ttc';//字体路径
        $url = dirname($_SERVER['SCRIPT_FILENAME']) . '/images/tj/title_pie.png';
        $position = 0;
        $j = 200;
        foreach ($arr as $son) {
            foreach ($son as $key => $nums) {
                $color = imageColorAllocate($img, rand(0, 255), rand(0, 255), rand(0, 255));
                $tratio = $nums / $allnums;
                $arc = round(360 * $tratio);
                $text = $key . '(' . intval($tratio * 100) . '%)';
                imageSetThickness($img, 1);
                if (($position + $arc) > 360) {
                    $temp = 360;
                } else {
                    $temp = $position + $arc;
                }
                imagefilledarc($img, $w / 2, $h / 2, 450, 450, $position, $temp, $color, IMG_ARC_PIE);
                imageSetThickness($img, 10);
                imageLine($img, 800, $j, 800, 10 + $j, $color);
                imagettftext($img, 10, 0, 820, 10 + $j, $col, $font, $text);
            }
            $position = $position + $arc;
            if ($position > 360) {
            }
            $j += 30;
        }
        header("Content-Type:image/png");
        imagepng($img, $url);
        imagedestroy($img);
    }

    /**
     * 创建缩略图
     * @param  $width
     * @param  $height
     * @param $name
     * @param  $src
     */
    function createThumbImg($width, $height, $name, $src)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $src;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $newsrc = substr($src, 0, strrpos($src, '\/'));
        $config['new_image'] = $newsrc;
        $config['width'] = 709;
        $config['height'] = 472;
        $config['thumb_marker'] = '_test';
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }
}