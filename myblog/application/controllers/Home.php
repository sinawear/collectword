<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('user_model');

		$this->cur_user =$this->user_model->is_login();
		if($this->cur_user === false){
			header("location:".site_url("login"));
		}else{
			//如果已经登陆，则重新设置cookie的有效期
			$this->input->set_cookie("username",$this->cur_user['username'],3600);
			$this->input->set_cookie("password",$this->cur_user['password'],3600);
			$this->input->set_cookie("user_id",$this->cur_user['user_id'],3600);
		}


	}

	public function index()
	{
		//$this->output->enable_profiler(TRUE);


		$data['title'] = "首页";

		if($this->cur_user === false){
			$data['isLogin'] = false;
		}else{
			$data['isLogin'] = true;
			$data['username'] = $this->cur_user['username'];
		}

		var_dump($data);


		$this->load->view('home', $data);
	}



}
