<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>我的知识库</title>

    <style type="text/css">

        ::selection { background-color: #E13300; color: white; }
        ::-moz-selection { background-color: #E13300; color: white; }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }



        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }


        #header {
            background-color: #f9f9f9;
            color: #002166;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
            text-align:right;

            height: 150px;

        }

        #action_bar {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #left {
            margin: 0px 15px 0px 15px;
            padding: 15px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;


            float: left;
        }

        #right {
            margin: 0 15px 0 15px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;

            width: 800px;
            float: left;
        }

        #logo_name {
            float: left;
        }

        #search{

        }



        p.footer {
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container {

        }
    </style>

    <script type="text/javascript" src="../../res/app.js"></script>

</head>
<body>

<div id="container">
   <h1>知识库名称</h1>


    <div id="body">
        <!--		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>-->
        <!--		<code>application/views/welcome_message.php</code>-->

        <div id="left">


            <?php
            /*
            //var_dump($list_container_left);
            for ($row = 0; $row < sizeof($list_container_left); $row++) {
                echo "<p><b>".$list_container_left[$row][0]."</b></p>";
                echo "<ul>";
                for ($col = 1; $col < sizeof($list_container_left[$row]); $col++) {
                    echo "<li>".$list_container_left[$row][$col]."</li>";
                }
                echo "</ul>";
            }
            //echo $title;
            //var_dump($content);
            */
            ?>


            <!--
            <ul>
                <li>标题1
                    <ul>
                    <li>子标题1</li>
                    <li>子标题2</li>
                    </ul>
                </li>
                <li>标题2
                    <ul>
                        <li>子标题1</li>
                        <li>子标题2</li>
                    </ul>
                </li>
                <li>饮食
                    <ul>
                        <li> 茶
                            <ul><li>红茶</li></ul>
                            <ul><li>绿茶</li></ul>
                        </li>
                        <li>主食
                            <ul><li>米饭</li></ul>
                            <ul>
                                <li>汤类
                                <ul>
                                    <li>西红柿鸡蛋汤</li>
                                    <li>排骨汤</li>
                                </ul>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </li>
            </ul>
            -->

        </div>

        <div id="right">
            内容

        </div>

    </div>

</div>

</body>
</html>