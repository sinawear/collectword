<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();

/**
 * 接口提供公共功能
 * Enter description here ...
 * @author Administrator
 *
 */
class Fuc extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('demo');

        $this->load->model(array('Mlog', 'Mfuc'));
    }


    /**
     * 错误日志记录 post
     */
    public function log_post()
    {

        $userId = $this->post('userId');
        $userName = $this->post('userName');
        $content = $this->post('content');
        /*
          if(!$content){
              $data = $this->formatData(1, '参数无效', '');
              $this->response($data, 200);
          }
        */
        $ip = $this->Mlog->get_ip();

        date_default_timezone_set("Asia/Shanghai");
        $time = date('Y-m-d H:i:s', time());
        $data = array(
            'content' => $content,
            'addTime' => $time,
            'ip' => $ip,
            'userId' => $userId, //'user_id'=>'userid'
            'userName' => $userName
        );

        $log = $this->Mlog->add_log($data);

        if ($log) {
            $data = $this->formatData(0, '提交成功', '');
            $this->response($data, 200);
        } else {
            $data = $this->formatData(1, '提交失败', '');
            $this->response($data, 200);
        }
    }


    public function index_get()
    {

        date_default_timezone_set("Asia/Shanghai");


        $time_now = strtotime('now');
        $result['TimeNow'] = $time_now * 1000;


        $time_now_format = date("Y-m-d H:i:s");
        $result['TimeNowFormat'] = $time_now_format;

        ///*
        //if($result)
        {
            $data = $this->formatData(0, '成功', $result);
            $this->response($data, 200);
        }
        //*/

    }


    /* failed
    public function phpinfo_get(){
        phpinfo();
    }
    */


}

?>
