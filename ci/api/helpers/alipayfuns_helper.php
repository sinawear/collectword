<?php
/* *
 * 支付宝接口公用函数
 * 详细：该类是请求、通知返回两个文件所调用的公用函数核心处理文件
 * 版本：3.2
 * 日期：2011-03-25
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */
/**
 * 生成签名结果
 * @param $sort_para 要签名的数组
 * @param $key 支付宝交易安全校验码
 * @param $sign_type 签名类型 默认值：MD5
 * return 签名结果字符串
 */
function buildMysign($sort_para, $key, $sign_type = "MD5")
{
    //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
    $prestr = createLinkstring($sort_para);
    //把拼接后的字符串再与安全校验码直接连接起来
    $prestr = $prestr . $key;
    //把最终的字符串签名，获得签名结果
    $mysgin = sign($prestr, $sign_type);
    return $mysgin;
}

/**
 * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
 * @param $para 需要拼接的数组
 * return 拼接完成以后的字符串
 */
function createLinkstring($para)
{
    $arg = "";
    while (list ($key, $val) = each($para)) {
        $arg .= $key . "=" . $val . "&";
    }
    //去掉最后一个&字符
    $arg = substr($arg, 0, count($arg) - 2);

    //如果存在转义字符，那么去掉转义
    if (get_magic_quotes_gpc()) {
        $arg = stripslashes($arg);
    }

    return $arg;
}

/**
 * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，并对字符串做urlencode编码
 * @param $para 需要拼接的数组
 * return 拼接完成以后的字符串
 */
function createLinkstringUrlencode($para)
{
    $arg = "";
    while (list ($key, $val) = each($para)) {
        $arg .= $key . "=" . urlencode($val) . "&";
    }
    //去掉最后一个&字符
    $arg = substr($arg, 0, count($arg) - 2);

    //如果存在转义字符，那么去掉转义
    if (get_magic_quotes_gpc()) {
        $arg = stripslashes($arg);
    }

    return $arg;
}

/**
 * 除去数组中的空值和签名参数
 * @param $para 签名参数组
 * return 去掉空值与签名参数后的新签名参数组
 */
function paraFilter($para)
{
    $para_filter = array();
    foreach ($para as $key => $val) {
        if ($key == "sign" || $key == "sign_type" || $val == "") continue;
        else    $para_filter[$key] = $para[$key];
    }
    return $para_filter;
}

/**
 * 对数组排序
 * @param $para 排序前的数组
 * return 排序后的数组
 */
function argSort($para)
{
    ksort($para);
    reset($para);
    return $para;
}

/**
 * 签名字符串
 * @param $prestr 需要签名的字符串
 * @param $sign_type 签名类型 默认值：MD5
 * return 签名结果
 */
function sign($prestr, $sign_type = 'MD5')
{
    $sign = '';
    if ($sign_type == 'MD5') {
        $sign = md5($prestr);
    } elseif ($sign_type == 'DSA') {
        //DSA 签名方法待后续开发
        die("DSA 签名方法待后续开发，请先使用MD5签名方式");
    } else {
        die("支付宝暂不支持" . $sign_type . "类型的签名方式");
    }
    return $sign;
}

/**
 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
 * 注意：服务器需要开通fopen配置
 * @param $word 要写入日志里的文本内容 默认值：空值
 */
function logResult($word = '')
{
    $fp = fopen("log.txt", "a");
    flock($fp, LOCK_EX);
    fwrite($fp, "执行日期：" . strftime("%Y%m%d%H%M%S", time()) . "\n" . $word . "\n");
    flock($fp, LOCK_UN);
    fclose($fp);
}

/**
 * 远程获取数据
 * 注意：该函数的功能可以用curl来实现和代替。curl需自行编写。
 * $url 指定URL完整路径地址
 * @param $input_charset 编码格式。默认值：空值
 * @param $time_out 超时时间。默认值：60
 * return 远程输出的数据
 */
function getHttpResponse($url, $input_charset = '', $time_out = "60")
{
    $urlarr = parse_url($url);
    $errno = "";
    $errstr = "";
    $transports = "";
    $responseText = "";
    if ($urlarr["scheme"] == "https") {
        $transports = "ssl://";
        $urlarr["port"] = "443";
    } else {
        $transports = "tcp://";
        $urlarr["port"] = "80";
    }
    $fp = @fsockopen($transports . $urlarr['host'], $urlarr['port'], $errno, $errstr, $time_out);
    if (!$fp) {
        die("ERROR: $errno - $errstr<br />\n");
    } else {
        if (trim($input_charset) == '') {
            fputs($fp, "POST " . $urlarr["path"] . " HTTP/1.1\r\n");
        } else {
            fputs($fp, "POST " . $urlarr["path"] . '?_input_charset=' . $input_charset . " HTTP/1.1\r\n");
        }
        fputs($fp, "Host: " . $urlarr["host"] . "\r\n");
        fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
        fputs($fp, "Content-length: " . strlen($urlarr["query"]) . "\r\n");
        fputs($fp, "Connection: close\r\n\r\n");
        fputs($fp, $urlarr["query"] . "\r\n\r\n");
        while (!feof($fp)) {
            $responseText .= @fgets($fp, 1024);
        }
        fclose($fp);
        $responseText = trim(stristr($responseText, "\r\n\r\n"), "\r\n");

        return $responseText;
    }
}

/**
 * 实现多种字符编码方式
 * @param $input 需要编码的字符串
 * @param $_output_charset 输出的编码格式
 * @param $_input_charset 输入的编码格式
 * return 编码后的字符串
 */
function charsetEncode($input, $_output_charset, $_input_charset)
{
    $output = "";
    if (!isset($_output_charset)) $_output_charset = $_input_charset;
    if ($_input_charset == $_output_charset || $input == null) {
        $output = $input;
    } elseif (function_exists("mb_convert_encoding")) {
        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
    } elseif (function_exists("iconv")) {
        $output = iconv($_input_charset, $_output_charset, $input);
    } else die("sorry, you have no libs support for charset change.");
    return $output;
}

/**
 * 实现多种字符解码方式
 * @param $input 需要解码的字符串
 * @param $_output_charset 输出的解码格式
 * @param $_input_charset 输入的解码格式
 * return 解码后的字符串
 */
function charsetDecode($input, $_input_charset, $_output_charset)
{
    $output = "";
    if (!isset($_input_charset)) $_input_charset = $_input_charset;
    if ($_input_charset == $_output_charset || $input == null) {
        $output = $input;
    } elseif (function_exists("mb_convert_encoding")) {
        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
    } elseif (function_exists("iconv")) {
        $output = iconv($_input_charset, $_output_charset, $input);
    } else die("sorry, you have no libs support for charset changes.");
    return $output;
}


/**
 * 通过节点路径返回字符串的某个节点值
 * $res_data——XML 格式字符串
 * 返回节点参数
 */
function getDataForXML($res_data, $node)
{
    $xml = simplexml_load_string($res_data);
    $result = $xml->xpath($node);

    while (list(, $node) = each($result)) {
        return $node;
    }
}

class AlipaySubmit
{
    /**
     * 生成要请求给支付宝的参数数组
     * @param $para_temp 请求前的参数数组
     * @param $alipay_config 基本配置信息数组
     * @return 要请求的参数数组
     */
    function buildRequestPara($para_temp, $alipay_config)
    {
        //除去待签名参数数组中的空值和签名参数
        $para_filter = paraFilter($para_temp);

        //对待签名参数数组排序
        $para_sort = argSort($para_filter);

        //生成签名结果
        $mysign = buildMysign($para_sort, trim($alipay_config['key']), strtoupper(trim($alipay_config['sign_type'])));

        //签名结果与签名方式加入请求提交参数组中
        $para_sort['sign'] = $mysign;
        $para_sort['sign_type'] = strtoupper(trim($alipay_config['sign_type']));

        return $para_sort;
    }

    /**
     * 生成要请求给支付宝的参数数组
     * @param $para_temp 请求前的参数数组
     * @param $alipay_config 基本配置信息数组
     * @return 要请求的参数数组字符串
     */
    function buildRequestParaToString($para_temp, $alipay_config)
    {
        //待请求参数数组
        $para = $this->buildRequestPara($para_temp, $alipay_config);

        //把参数组中所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，并对参数值做urlencode编码
        $request_data = createLinkstringUrlencode($para);

        return $request_data;
    }

    /**
     * 构造提交表单HTML数据
     * @param $para_temp 请求参数数组
     * @param $gateway 网关地址
     * @param $method 提交方式。两个值可选：post、get
     * @param $button_name 确认按钮显示文字
     * @return 提交表单HTML文本
     */
    function buildForm($para_temp, $gateway, $method, $button_name, $alipay_config)
    {
        //待请求参数数组
        $para = $this->buildRequestPara($para_temp, $alipay_config);

        $sHtml = "<form id='alipaysubmit' name='alipaysubmit' action='" . $gateway . "_input_charset=" . trim(strtolower($alipay_config['input_charset'])) . "' method='" . $method . "'>";
        while (list ($key, $val) = each($para)) {
            $sHtml .= "<input type='hidden' name='" . $key . "' value='" . $val . "'/>";
        }

        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml . "<input type='submit' value='" . $button_name . "'></form>";

        $sHtml = $sHtml . "<script>document.forms['alipaysubmit'].submit();</script>";

        return $sHtml;
    }

    /**
     * 构造模拟远程HTTP的POST请求，获取支付宝的返回XML处理结果
     * 注意：该功能PHP5环境及以上支持，因此必须服务器、本地电脑中装有支持DOMDocument、SSL的PHP配置环境。建议本地调试时使用PHP开发软件
     * @param $para_temp 请求参数数组
     * @param $gateway 网关地址
     * @param $alipay_config 基本配置信息数组
     * @return 支付宝返回XML处理结果
     */
    function sendPostInfo($para_temp, $gateway, $alipay_config)
    {
        $xml_str = '';

        //待请求参数数组字符串
        $request_data = $this->buildRequestParaToString($para_temp, $alipay_config);
        //请求的url完整链接
        $url = $gateway . $request_data;
        //远程获取数据
        $xml_data = getHttpResponse($url, trim(strtolower($alipay_config['input_charset'])));
        //解析XML
        $doc = new DOMDocument();
        $doc->loadXML($xml_data);

        return $doc;
    }
}

class AlipayService
{

    var $alipay_config;
    /**
     *支付宝网关地址（新）
     */
    var $alipay_gateway_new = 'https://mapi.alipay.com/gateway.do?';

    function __construct($alipay_config)
    {
        $this->alipay_config = $alipay_config;
    }

    function AlipayService($alipay_config)
    {
        $this->__construct($alipay_config);
    }

    /**
     * 构造即时到帐接口
     * @param $para_temp 请求参数数组
     * @return 表单提交HTML信息
     */
    function create_direct_pay_by_user($para_temp)
    {
        //设置按钮名称
        $button_name = "确认";
        //生成表单提交HTML文本信息
        $alipaySubmit = new AlipaySubmit();
        $html_text = $alipaySubmit->buildForm($para_temp, $this->alipay_gateway_new, "get", $button_name, $this->alipay_config);

        return $html_text;
    }

    /**
     * 用于防钓鱼，调用接口query_timestamp来获取时间戳的处理函数
     * 注意：该功能PHP5环境及以上支持，因此必须服务器、本地电脑中装有支持DOMDocument、SSL的PHP配置环境。建议本地调试时使用PHP开发软件
     * return 时间戳字符串
     */
    function query_timestamp()
    {
        $url = $this->alipay_gateway_new . "service=query_timestamp&partner=" . trim($this->alipay_config['partner']);
        $encrypt_key = "";

        $doc = new DOMDocument();
        $doc->load($url);
        $itemEncrypt_key = $doc->getElementsByTagName("encrypt_key");
        $encrypt_key = $itemEncrypt_key->item(0)->nodeValue;

        return $encrypt_key;
    }

    /**
     * 构造支付宝其他接口
     * @param $para_temp 请求参数数组
     * @return 表单提交HTML信息/支付宝返回XML处理结果
     */
    function alipay_interface($para_temp)
    {
        //获取远程数据
        $alipaySubmit = new AlipaySubmit();
        $html_text = "";
        //请根据不同的接口特性，选择一种请求方式
        //1.构造表单提交HTML数据:（$method可赋值为get或post）
        //$alipaySubmit->buildForm($para_temp, $this->alipay_gateway, "get", $button_name,$this->alipay_config);
        //2.构造模拟远程HTTP的POST请求，获取支付宝的返回XML处理结果:
        //注意：若要使用远程HTTP获取数据，必须开通SSL服务，该服务请找到php.ini配置文件设置开启，建议与您的网络管理员联系解决。
        //$alipaySubmit->sendPostInfo($para_temp, $this->alipay_gateway, $this->alipay_config);

        return $html_text;
    }
}

class AlipayNotify
{
    /**
     * HTTPS形式消息验证地址
     */
    var $https_verify_url = 'https://mapi.alipay.com/gateway.do?service=notify_verify&';
    /**
     * HTTP形式消息验证地址
     */
    var $http_verify_url = 'http://notify.alipay.com/trade/notify_query.do?';
    var $alipay_config;

    function __construct($alipay_config)
    {
        $this->alipay_config = $alipay_config;
    }

    function AlipayNotify($alipay_config)
    {
        $this->__construct($alipay_config);
    }

    /**
     * 针对notify_url验证消息是否是支付宝发出的合法消息
     * @return 验证结果
     */
    function verifyNotify()
    {
        if (empty($_POST)) {//判断POST来的数组是否为空
            return false;
        } else {
            //生成签名结果
            $mysign = $this->getMysign($_POST);
            //获取支付宝远程服务器ATN结果（验证是否是支付宝发来的消息）
            $responseTxt = 'true';
            if (!empty($_POST["notify_id"])) {
                $responseTxt = $this->getResponse($_POST["notify_id"]);
            }

            //写日志记录
            //$log_text = "responseTxt=".$responseTxt."\n notify_url_log:sign=".$_POST["sign"]."&mysign=".$mysign.",";
            //$log_text = $log_text.createLinkString($_POST);
            //logResult($log_text);

            //验证
            //$responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
            //mysign与sign不等，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
            if (preg_match("/true$/i", $responseTxt) && $mysign == $_POST["sign"]) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 针对return_url验证消息是否是支付宝发出的合法消息
     * @return 验证结果
     */
    function verifyReturn()
    {
        if (empty($_GET)) {//判断POST来的数组是否为空
            return false;
        } else {
            //生成签名结果
            $mysign = $this->getMysign($_GET);
            //获取支付宝远程服务器ATN结果（验证是否是支付宝发来的消息）
            $responseTxt = 'true';
            if (!empty($_GET["notify_id"])) {
                $responseTxt = $this->getResponse($_GET["notify_id"]);
            }

            //写日志记录
            //$log_text = "responseTxt=".$responseTxt."\n notify_url_log:sign=".$_GET["sign"]."&mysign=".$mysign.",";
            //$log_text = $log_text.createLinkString($_GET);
            //logResult($log_text);

            //验证
            //$responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
            //mysign与sign不等，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
            if (preg_match("/true$/i", $responseTxt) && $mysign == $_GET["sign"]) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 根据反馈回来的信息，生成签名结果
     * @param $para_temp 通知返回来的参数数组
     * @return 生成的签名结果
     */
    function getMysign($para_temp)
    {
        //除去待签名参数数组中的空值和签名参数
        $para_filter = paraFilter($para_temp);

        //对待签名参数数组排序
        $para_sort = argSort($para_filter);

        //生成签名结果
        $mysign = buildMysign($para_sort, trim($this->alipay_config['key']), strtoupper(trim($this->alipay_config['sign_type'])));

        return $mysign;
    }

    /**
     * 获取远程服务器ATN结果,验证返回URL
     * @param $notify_id 通知校验ID
     * @return 服务器ATN结果
     * 验证结果集：
     * invalid命令参数不对 出现这个错误，请检测返回处理中partner和key是否为空
     * true 返回正确信息
     * false 请检查防火墙或者是服务器阻止端口问题以及验证时间是否超过一分钟
     */
    function getResponse($notify_id)
    {
        $transport = strtolower(trim($this->alipay_config['transport']));
        $partner = trim($this->alipay_config['partner']);
        $veryfy_url = '';
        if ($transport == 'https') {
            $veryfy_url = $this->https_verify_url;
        } else {
            $veryfy_url = $this->http_verify_url;
        }
        $veryfy_url = $veryfy_url . "partner=" . $partner . "&notify_id=" . $notify_id;
        $responseTxt = getHttpResponse($veryfy_url);

        return $responseTxt;
    }
}

/**
 *类名：alipay_service
 *功能：支付宝Wap服务接口控制
 *详细：该页面是请求参数核心处理文件，不需要修改
 *版本：2.0
 */
class Alipay_Service
{
    // 线上
    var $gateway_order = "http://wappaygw.alipay.com/service/rest.htm?";
    var $_key; // 安全校验码
    var $mysign; // 签名结果
    var $sign_type; // 签名类型 相当于config文件中的sec_id
    var $parameter; // 需要签名的参数数组
    var $format; // 字符编码格式
    var $req_data = ''; // post请求数据

    /**
     * 构造函数
     */
    function Alipay_Service()
    {
    }

    /**
     * 创建alipay.wap.trade.create.direct接口
     */
    function alipay_wap_trade_create_direct($parameter, $key, $sign_type)
    {
        $this->_key = $key; // MD5校验码
        $this->sign_type = $sign_type; // 签名类型，此处为MD5
        $this->parameter = paraFilter($parameter); // 除去数组中的空值和签名参数
        $this->req_data = $parameter ['req_data'];
        $this->format = $this->parameter['format']; // 编码格式，此处为utf-8
        $sort_array = argSort($this->parameter); // 得到从字母a到z排序后的签名参数数组
        $this->mysign = buildMysign($sort_array, $this->_key, $this->sign_type); // 生成签名
        $this->req_data = createLinkstring($this->parameter) . '&sign=' . urlencode($this->mysign); // 配置post请求数据，注意sign签名需要urlencode

        // Post提交请求
        $result = $this->post($this->gateway_order);
        // 调用GetToken方法，并返回token
        return $this->getToken($result);
    }

    /**
     * 调用alipay_Wap_Auth_AuthAndExecute接口
     */
    function alipay_Wap_Auth_AuthAndExecute($parameter, $key)
    {
        $this->parameter = paraFilter($parameter);
        $sort_array = argSort($this->parameter);
        $this->sign_type = $this->parameter['sec_id'];
        $this->_key = $key;
        $this->mysign = buildMysign($sort_array, $this->_key, $this->sign_type);
        $RedirectUrl = $this->gateway_order . createLinkstring($this->parameter) . '&sign=' . urlencode($this->mysign);
        //$RedirectUrl ='http://wappaygw.alipay.com/service/rest.htm?req_data=<auth_and_execute_req><request_token>20121112f7f45b5fddadf9f1ba62bf77b3f18275</request_token></auth_and_execute_req>&service=alipay.wap.auth.authAndExecute&sec_id=MD5&partner=2088801703446485&call_back_url=www.twst.com&format=xml&v=2.0&sign=a9e50cfefe7dd7f32aea9af57f55765c';
        // 跳转至该地址
        Header("Location:$RedirectUrl");
//echo '<script>Location.href="$RedirectUrl"</script>';

    }

    /**
     * 返回token参数
     * 参数 result 需要先urldecode
     */
    function getToken($result)
    {
        $result = urldecode($result); // URL转码
        $Arr = explode('&', $result); // 根据 & 符号拆分

        $temp = array(); // 临时存放拆分的数组
        $myArray = array(); // 待签名的数组
        // 循环构造key、value数组
        for ($i = 0; $i < count($Arr); $i++) {
            $temp = explode('=', $Arr [$i], 2);
            $myArray [$temp [0]] = $temp [1];
        }

        @$sign = $myArray ['sign']; // 支付宝返回签名
        $myArray = paraFilter($myArray); // 拆分完毕后的数组

        $sort_array = argSort($myArray); // 排序数组
        $this->mysign = buildMysign($sort_array, $this->_key, $this->sign_type); // 构造本地参数签名，用于对比支付宝请求的签名

        if ($this->mysign == $sign) {// 判断签名是否正确
            return getDataForXML($myArray ['res_data'], '/direct_trade_create_res/request_token'); // 返回token
        } else {
            echo('签名不正确'); // 当判断出签名不正确，请不要验签通过
            return '签名不正确';
        }
    }

    /**
     * PHP Crul库 模拟Post提交至支付宝网关
     * 如果使用Crul 你需要改一改你的php.ini文件的设置，找到php_curl.dll去掉前面的";"就行了
     * 返回 $data
     */
    function post($gateway_url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $gateway_url); // 配置网关地址
        curl_setopt($ch, CURLOPT_HEADER, 0); // 过滤HTTP头
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1); // 设置post提交
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->req_data); // post传输数据
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}

/*
 *类名：Alipay_Notify
 *功能：付款过程中服务器通知类
 *详细：该页面是通知返回核心处理文件，不需要修改
 *版本：2.0
*/

////////////////////注意/////////////////////////
//调试通知返回时，可查看或改写log日志的写入TXT里的数据，来检查通知返回是否正常
/////////////////////////////////////////////////

class Alipay_Notify
{
    var $gateway;           //网关地址
    var $_key;                //安全校验码
    var $partner;           //合作伙伴ID
    var $sign_type;         //签名方式 系统默认
    var $mysign;            //签名结果
    var $_input_charset;    //字符编码格式

    /**构造函数
     * 从配置文件中初始化变量
     * $partner 合作身份者ID
     * $key 安全校验码
     * $sign_type 签名类型
     * $_input_charset 字符编码格式
     */
    function Alipay_Notify($partner, $key, $sign_type, $_input_charset)
    {
        $this->gateway = "http://wappaygw.alipay.com/service/rest.htm?";

        $this->partner = $partner;
        $this->_key = $key;
        $this->mysign = "";
        $this->sign_type = $sign_type;
        $this->_input_charset = $_input_charset;
    }

    /********************************************************************************/

    /**对notify_url的认证
     *返回的验证结果：true/false
     */
    function notify_verify()
    {
        //判断POST来的数组是否为空
        if (empty($_POST)) {
            return false;
        } else {
            //此处为固定顺序，支付宝Notify返回消息通知比较特殊，这里不需要升序排列
            $notifyarray = array(
                "service" => $_POST['service'],
                "v" => $_POST['v'],
                "sec_id" => $_POST['sec_id'],
                "notify_data" => $_POST['notify_data']
            );

            $this->mysign = buildMysign($notifyarray, $this->_key, $this->sign_type);
            //记录日志（调试用）
            //log_result($this->mysign . ' ' . $_POST["sign"]);
            //判断veryfy_result是否为ture，生成的签名结果mysign与获得的签名结果sign是否一致
            //mysign与sign不等，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
            if ($this->mysign == $_POST["sign"]) {
                return true;
            } else {
                return false;
            }
        }
    }

    /********************************************************************************/

    /**对return_url的认证
     *return 验证结果：true/false
     */
    function return_verify()
    {
        //判断GET来的数组是否为空
        if (empty($_GET)) {
            return false;
        } else {
            $get = paraFilter($_GET);        //对所有GET反馈回来的数据去空
            $sort_get = argSort($get);            //对所有GET反馈回来的数据排序
            $this->mysign = buildMysign($sort_get, $this->_key, $this->sign_type);    //生成签名结果
            //print_r($this->mysign);
            if ($this->mysign == $_GET["sign"]) {
                return true;
            } else {
                return false;
            }
        }
    }
}