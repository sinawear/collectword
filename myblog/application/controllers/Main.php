<?php

/**
 * Created by PhpStorm.
 * User: matengfei
 * Date: 16/1/14
 * Time: 下午12:12
 */
class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model');

        $this->cur_user=$this->user_model->is_login();
        if($this->cur_user === false){
            header("location:".site_url("login"));
        }else{
            //如果已经登陆，则重新设置cookie的有效期
            $this->input->set_cookie("username",$this->cur_user['username'],3600);
            $this->input->set_cookie("password",$this->cur_user['password'],3600);
            $this->input->set_cookie("user_id",$this->cur_user['user_id'],3600);
        }


    }


    public function index()
    {


    }


}