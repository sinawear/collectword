<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>我的知识库</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}



	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}


	#header {
		background-color: #f9f9f9;
		color: #002166;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
		text-align:right;

		height: 150px;

	}

	#action_bar {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#left {
		margin: 0px 15px 0px 15px;
		padding: 15px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;


		float: left;
	}

	#right {
		margin: 0 15px 0 15px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;

		width: 800px;
		float: left;
	}

	#logo_name {
		float: left;
	}

	#search{

	}

	.block_content{
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;

		text-align: center;
	}


	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {

	}
	</style>


</head>
<body>

<div id="container">
<!--	<h1>知识库名称</h1>-->

	<div id="header">
		<div id="logo_name">
			<img src="res/logo_apple.jpg" width="128px" height="128px"/>
			<?php echo $title ?>
		</div>

		<div id="div_login_register" <?php if($isLogin) echo "style=\"display:none\"" ; ?> >
			<a id="login" href="<?php echo site_url('login')?>">登陆</a>
			<a id="register" href="<?php echo site_url('register')?>">注册</a>
		</div>

		<div id="div_alreay_login" <?php if(!$isLogin) echo "style=\"display:none\"" ; ?> >
			<a id="username" href=""><?php echo $username ?></a>
		</div>


		<div id="search">

			<form action="<?php echo site_url('search')?>"/>
			<input type="text" id="search_word" placeholder="比如:"/>
			<input type="submit" value="搜索" />
			</form>


		</div>

	</div>

	<div id="action_bar">


	</div>


	<div id="body">



	</div>

</div>

</body>
</html>