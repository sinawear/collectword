-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2016-01-13 09:38:56
-- 服务器版本： 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `collectword`
--

-- --------------------------------------------------------

--
-- 表的结构 `t_word`
--

CREATE TABLE IF NOT EXISTS `t_word` (
  `raw_word` text NOT NULL,
  `raw_explain` text NOT NULL,
  `raw_result` text NOT NULL,
  `add_time` int(11) NOT NULL,
  `add_time_str` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_word`
--

INSERT INTO `t_word` (`raw_word`, `raw_explain`, `raw_result`, `add_time`, `add_time_str`) VALUES
('result', '结果', '{"from":"en","to":"zh","trans_result":[{"src":"result","dst":"u7ed3u679c"}]}', 1452674314, '2016-01-13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
