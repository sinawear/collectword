<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//公共函数类
class Common
{
    function __construct()
    {
    }

    function success($msg, $http, $time = 3)
    {
        $js = <<<JS
		<!doctype html>
		<html>
			<head>
				<title>页面跳转</title>
				<meta http-equiv='content-type' content='text/html;charset=utf-8'>
				<style type='text/css'>
					*{margin:0px auto; padding:0px;}
					.main{ width:800px; height:480px; text-align:center; }
					.notice{ width:500px; height:300px; border:1px solid blue; margin-top:80px}
					.notice h4{
						color:white;
						display:block;
						width:500px;
						height:50px;
						background:blue;
						line-height:50px;
						font-size:24px;
					}
					.notice2{width:500px; height:200px; font-size:24px;}
					.notice a:link{
						text-decoration:none;
						color:black;
					}
					.notice a:hover{
						text-decoration:underline;
						color:red;
					}
				
				</style>
			</head>
			<body>
				<div class='main'>
					<div class="notice">
					<h4>{$msg}</h4>
					<div class='notice2'><br/><br/><a href='#' name='a1'>{$msg}!<br/><br/>页面<span style='color:red' id='time'>{$time}</span>秒之后跳转,页面无响应点击此处</a></div>
					<div>
				</div>
				<script type='text/javascript'>
					var second = '{$time}';
					function timeout(){
						second--;
						var obj = document.getElementById('time');
						if(second >0){
							obj.innerHTML = second;
							document.getElementsByName('a1')[0].onclick = function(){
								window.location.href='{$http}';
							}
						}else{
							clearInterval(t);
							window.location.href='{$http}';
						}
					}
					var t = setInterval('timeout()',1000);
				</script>
			</body>
		</html>
JS;
        echo $js;
    }

    function error()
    {
    }

}

?>